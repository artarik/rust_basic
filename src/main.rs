fn print_type_of<T>(_: &T) {
    println!("Тип переменной: {}", std::any::type_name::<T>())
}

fn double_int32(x: i32) -> Option<i32>  {
    x.checked_mul(2)
}

fn double_int64(x:i32) -> Option<i64>{
    (x as i64).checked_mul(2)
}


fn double_float32(x: f32) -> f32 {
    x * 2.0
}

fn double_float64(x:f32) -> f64 {
    (x as f64) * 2.0
}

fn int_plus_float_to_float(x: i32, y: f32) -> f64 {
    (x as f64) + (y as f64)
}

fn int_plus_float_to_int(x:i32, y:f32) ->i64 {
    (x + (y as i32)).into()
}

fn tuple_sum(tuple: (i32, i32)) -> i64 {
    (tuple.0 + tuple.1).into()
}


fn array_sum(arr: &[i32;3]) -> i64{
    let mut sum = 0;
    for &num in arr {
        sum += num;
    }
    sum.into()

}

fn main() {
// double_int32
    println!("Task.1 Функция double_int32 принимает 32-х битное целое знаковое число\nи возвращает 32-х битное целое знаковое число, равное удвоенному входному.\n");
    let x: i32 = 3_000_2222;
    match double_int32(x) {
        Some(result) => println!("Результат умножения {} на 2: {}",x, result),
        None => println!("Err:Произведение больше чем максимальное значение i32")
    }
    let x: i32 = 129_512_1280;
    match double_int32(x) {
        Some(result) => println!("Результат умножения {} на 2: {}",x, result),
        None => println!("Err:Произведение {} на 2 больше чем максимальное значение i32", x)
    }
    println!("\nTask.2 Функция double_int64 принимает 32-х битное целое знаковое число\nи возвращает 64-х битное целое знаковое число, равное удвоенному входному\n");
    let x:i32 = 213_512_1280;
    match double_int64(x) {
        Some(result) => {
            println!("Результат умножения {} на 2: {}",x, result);
            print_type_of(&result);
        }
        None => println!("Err:Произведение больше чем максимальное значение i32")
    }
    println!("\nTask.3 Функция double_float32 принимает 32-х битное число с плавающей точкой\n и возвращает 32-х битное число с плавающей точкой, равное удвоенному входному.\n");
    let x:f32 = 158.5;
    println!("Результат умножения {} на 2: {}",x, double_float32(x));

    println!("\nTask.4 Функция double_float64 принимает 32-х битное число с плавающей точкой\nи возвращает 64-х битное число с плавающей точкой, равное удвоенному входному.\n");
    let x:f32 = 1582.55;
    let y:f64 = double_float64(x);
    println!("Результат умножения {} на 2: {}",x, y);
    print_type_of(&y);

    println!("\nTask.5 Функция int_plus_float_to_float принимает 32-х битное целое знаковое число\nи 32-х битное число с плавающей точкой.\nВозвращает 64-х битное число с плавающей точкой, равное сумме входных.\n");
    let x:i32 = 1582;
    let y:f32 = 2568.54;
    let z:f64 = int_plus_float_to_float(x, y);
    println!("Результат сложения {} на {}: {}",x, y, z);
    print_type_of(&z);

    println!("\nTask.6 Функция int_plus_float_to_int принимает 32-х битное целое знаковое число и 32-х битное число с плавающей точкой. Возвращает 64-х битное целое знаковое число, равное сумме входных\n");
    let x:i32 = 1423412;
    let y:f32 = 23423.234;
    let z:i64 = int_plus_float_to_int(x, y);
    println!("Результат сложения {} на {}: {}",x, y, z);
    print_type_of(&z);

    println!("\nTask.7 Функция tuple_sum принимает кортеж из двух целых чисел. Возвращает целое число, равное сумме чисел во входном кортеже.\n");
    let x:i32 = 234321;
    let y:i32 = 242342343;
    let z = tuple_sum((x,y));
    println!("Результат сложения {} на {}: {}",x, y, z);

    println!("\nTask.8 Функция array_sum принимает массив из трёх целых чисел. Возвращает целое число, равное сумме чисел во входном массиве.\n");
    let arr:[i32; 3] = [123,423,23432];
    let res = array_sum(&arr);
    println!("Массив: {:?}", arr);
    println!("Результат сложения чисел в массиве: {}", res);

}
